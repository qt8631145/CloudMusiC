import QtQuick 2.15
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.15

ColumnLayout{

    property string targetId: ""
    property string targetType: "10"
    property string name: "-"

    onTargetIdChanged: {
        if(targetType=="10")
            loadAlbum()
        else if(targetType=="1000")
            loadPlayList()
    }

    Rectangle{
        Layout.fillWidth: true
        width: parent.width
        height: 40
        color: "#00000000"

        Text{
            x: 10
            verticalAlignment: Text.AlignBottom
            text: qsTr(targetType=="10"?"专辑":"歌单")
            font.family: window.mFONT_FAMILY
            font.pointSize: 25
        }
    }

    RowLayout{
        height: 200
        width: parent.width
        MusicRoundImage{
            id: playListCover
            width: 180
            height: 180
            Layout.leftMargin: 20
            imgSrc: "https://p2.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg"
        }

        Item{
            Layout.fillWidth: true
            height: parent.height

            Text{
                id: playListDesc
                text: "https://p2.music.126.net/6y-UleORITEDbvrOLV0Q8A==/5639395138885805.jpg"
                width: parent.width*0.95
                anchors.centerIn: parent
                wrapMode: Text.WrapAnywhere
                font.family: window.mFONT_FAMILY
                font.pointSize: 14
                maximumLineCount: 4
                elide: Text.ElideRight
                lineHeight: 1.5
            }
        }
    }

    MusicListView{
        id: playListListView
    }

    //专辑详情
    function loadAlbum(){
        var url = "album?id="+(targetId.length<1?"32311":targetId)
        function onReply(reply){ //执行getHotList时onReply不会调用，要收到信号，才会调用
            http.onReplySignal.disconnect(onReply) //断开
            var album = JSON.parse(reply).album
            var songs = JSON.parse(reply).songs
            playListCover.imgSrc = album.blurPicUrl
            playListDesc.text = album.description
            name = "-"+album.name
            playListListView.musicList = songs.map(item=>{
                                                       return {
                                                           id: item.id,
                                                           name: item.name,
                                                           artist: item.ar[0].name,
                                                           album: item.al.name
                                                        }
                                                    })
        }

        http.onReplySignal.connect(onReply)
        http.connet(url) //拿到专辑详情
    }


    //歌单详情
    function loadPlayList(){
        var url ="playlist/detail?id="+(targetId.length<1?"32311":targetId)

        function onSongDetailReply(reply){ //执行getHotList时onReply不会调用，要收到信号，才会调用
            http.onReplySignal.disconnect(onReply) //断开
            var playlist = JSON.parse(reply).playlist
            playListCover.imgSrc = playlist.coverImgUrl
            playListDesc.text = playlist.description
            name = "-"+playlist.name

            var ids = playlist.trackIds.map(item=>item.id)
        }

        function onReply(reply){ //执行getHotList时onReply不会调用，要收到信号，才会调用
            http.onReplySignal.disconnect(onReply) //断开
            var playlist = JSON.parse(reply).playlist
            playListCover.imgSrc = playlist.coverImgUrl
            playListDesc.text = playlist.description
            name = "-"+playlist.name

            var ids = playlist.trackIds.map(item=>item.id)

            http.onReplySignal.connect(onReply)
            http.connet("song/detail?ids=") //拿到歌单详情
        }

        http.onReplySignal.connect(onSongDetailReply)
        http.connet(url) //拿到专辑详情
    }









}











