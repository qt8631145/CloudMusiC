import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQml 2.15


//推荐首页下的推荐歌单
Item{

    property alias lists: gridRepeater.model

    Grid{
        id: gridLayout
        anchors.fill: parent
        columns: 5

        Repeater{
            id: gridRepeater
            Frame{
                padding: 10
                width: parent.width*0.2
                height: parent.width*0.2+30
                background: Rectangle{
                    id: backgroud
                    color: "#00000000"
                }
                clip: true

                MusicRoundImage{
                    id: img
                    width: parent.width
                    height: parent.width
                    imgSrc: modelData.coverImgUrl //Repeater里的modelData
                }

                Text{
                    anchors{
                        top: img.bottom
                        horizontalCenter: parent.horizontalCenter
                    }
                    text: modelData.name
                    font{
                        family: window.mFONT_FAMILY
                    }

                    height: 30
                    width: parent.width
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    elide: Qt.ElideMiddle //超出部分，自动省略
                }

                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true
                    cursorShape: Qt.PointingHandCursor
                    onEntered: {
                        backgroud.color = "#50000000"
                    }
                    onExited: {
                        backgroud.color = "#00000000"
                    }
                }
            }
        }
    }
}
