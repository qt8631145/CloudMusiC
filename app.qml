import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.15
import MyUtils 1.0 //命名空间引入

//主窗口
ApplicationWindow {

    id: window

    property int mWINDOW_WIDTH: 1200
    property int mWINDOW_HEIGHT: 700

    property string mFONT_FAMILY: "微软雅黑"

    width: mWINDOW_WIDTH
    height: mWINDOW_HEIGHT
    visible: true
    title: qsTr("Cloud Music")


    HttpUtils{
        id: http
    }




    ColumnLayout{
        anchors.fill: parent
        spacing: 0 //内边距

        //顶部工具栏
        LayoutHeaderView{
            id: layoutHeaderView
        }

        //中间页面
        PageHomeView{
            id: pageHomeView
        }

        //底部工具栏
        LayoutBottomView{
            id: layoutBottomView
        }

    }
















}










