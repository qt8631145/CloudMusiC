QT += quick

CONFIG += c++11

SOURCES += \
    app.cpp \
    httputils.cpp

RESOURCES += \
    images.qrc \
    qml.qrc

DISTFILES +=

HEADERS += \
    httputils.h
