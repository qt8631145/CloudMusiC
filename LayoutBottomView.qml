import QtQuick 2.15
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

//底部工具栏
Rectangle{
    Layout.fillWidth: true
    height: 55
    color: "#00AAAA"

    RowLayout{
        anchors.fill: parent

        Item{
            Layout.preferredWidth: parent.width/10
            Layout.fillWidth: true
        }
        MusicIconButton{
            iconSource: "qrc:/images/previous"
            iconWidth: 32
            iconHeight: 32
            toolTip: "上一首"
        }
        MusicIconButton{
            iconSource: "qrc:/images/stop"
            iconWidth: 32
            iconHeight: 32
            toolTip: "暂停/播放"
        }
        MusicIconButton{
            icon.source: "qrc:/images/next"
            iconWidth: 32
            iconHeight: 32
            toolTip: "下一首"
        }
        Item{
            Layout.preferredWidth: parent.width/2
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.topMargin: 25

            Text{
                id:nameText
                anchors.left: slider.left
                anchors.bottom: slider.top
                anchors.leftMargin: 2
                Layout.bottomMargin: 10
                text:"歌曲-歌手"
                font.family: window.mFONT_FAMILY
                color: "#ffffff"
            }
            Text{
                id:timeText
                anchors.right: slider.right
                anchors.bottom: slider.top
                anchors.rightMargin: 2
                Layout.bottomMargin: 10
                text: "00:00/05:30"
                font.family: window.mFONT_FAMILY
                color: "#ffffff"
            }

            Slider{
                id:slider
                width: parent.width
                Layout.fillWidth: true
                height: 25
                //进度条
                background: Rectangle{
                    //x,y：居中定位
                    x: slider.leftPadding
                    y: slider.topPadding+(slider.availableHeight-height)/2
                    width: slider.availableWidth
                    height: 4
                    radius: 2
                    color: "#e9f4ff"

                    //进度条handle拖过的部分更改的颜色
                    Rectangle{
                        width: slider.visualPosition*parent.width
                        height: parent.height
                        color: "#73a7ab"
                        radius: 2
                    }
                }
                //进度条的把手
                handle: Rectangle{
                    x: slider.leftPadding+(slider.availableWidth-width)*slider.visualPosition
                    y: slider.topPadding+(slider.availableHeight-height)/2
                    width: 15
                    height: 15
                    radius: 5
                    color: "#f0f0f0"
                    border.color: "#73a7ab"
                    border.width: 0.5
                }
            }
        }

        MusicIconButton{
            iconSource: "qrc:/images/favorite"
            iconWidth: 32
            iconHeight: 32
            toolTip: "我喜欢"
        }
        MusicIconButton{
            iconSource: "qrc:/images/repeat"
            iconWidth: 32
            iconHeight: 32
            toolTip: "循环播放"
        }
        Item{
            Layout.preferredWidth: parent.width/10
            Layout.fillWidth: true
        }
    }
}
