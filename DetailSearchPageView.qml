import QtQuick 2.15
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.15


ColumnLayout{
    Layout.fillWidth: true
    Layout.fillHeight: true


    //标题
    Rectangle{
        Layout.fillWidth: true
        width: parent.width
        height: 40
        color: "#00000000"

        Text{
            x: 10
            verticalAlignment: Text.AlignBottom
            text: qsTr("搜索音乐")
            font.family: window.mFONT_FAMILY
            font.pointSize: 25
        }

    }

    //搜索框
    RowLayout{
        Layout.fillWidth: true

        TextField{
            id: searchInput
            font.family: window.mFONT_FAMILY
            font.pointSize: 14
            selectByMouse: true
            selectionColor: "#999999" //选中后的颜色
            placeholderText: qsTr("请输入搜索关键词")
            color: "#000000"
            background: Rectangle{
                color: "#00000000"
                opacity: 0.5
                implicitHeight: 40
                implicitWidth: 400
            }

            focus: true

            //添加快捷键
            Keys.onPressed:
                if(event.key===Qt.Key_Enter||event.key===Qt.Key_Return)
                    doSearch()
        }

        MusicIconButton{
            iconSource: "qrc:/images/search"
            toolTip: "搜索"
            Layout.topMargin: 20
            onClicked: {
                doSearch()
            }
        }
    }

    MusicListView{
        id: musicListView
        onLoadMore: doSearch(offset, pageCurrent) //1.b接收信号
    }

    function doSearch(offset=0, pageCurrent=0){

        var keywords = searchInput.text
        if(keywords.length<1)
            return

        function onReply(reply){ //执行getHotList时onReply不会调用，要收到信号，才会调用

            http.onReplySignal.disconnect(onReply) //断开
            var result = JSON.parse(reply).result
            musicListView.pageCurrent = pageCurrent
            musicListView.all = result.songCount
            musicListView.musicList = result.songs.map(item=>{
                                                        return {
                                                            id: item.id,
                                                            name: item.name,
                                                            artist: item.artists[0].name,
                                                            album: item.album.name
                                                        }
                                                    })
        }

        http.onReplySignal.connect(onReply)
        http.connet("search?keywords="+keywords+"&offset="+offset+"&limit=60") //拿到搜索结果
    }





}







