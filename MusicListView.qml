import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.5
import QtQml 2.15
import QtQuick.Shapes 1.12

//搜索音乐的结果列表
Pane{

    property var musicList: []

    //分页
    property int all: 0
    property int pageSize: 60
    property int pageCurrent: 0


    //定义信号
    signal loadMore(int offset, int pageCurrent)


    onMusicListChanged: {
        listViewModel.clear()
        listViewModel.append(musicList)
    }

    Layout.fillHeight: true
    Layout.fillWidth: true
    clip: true //避免搜索结果滑动闪烁
    padding: 0 //消除边距
    background: Rectangle{
        color: "#00000000"
    }

    ListView{
        id: listView
        anchors.fill: parent
        anchors.bottomMargin: 60
        model: ListModel{
            id: listViewModel
        }

        delegate: listViewDelegate
        ScrollBar.vertical: ScrollBar{
            anchors.right: parent.right
        }
        header: listViewHeader

        //高亮
        highlight: Rectangle{
            color: "#20f0f0f0"
        }
        highlightMoveDuration: 0 //移动时不会有延时
        highlightResizeDuration: 0
    }

    //搜索结果
    Component{
        id: listViewDelegate

        Rectangle{
            id: listViewDelegateItem
            height: 45
            width: listView.width

            Shape{
                anchors.fill: parent
                ShapePath{
                    strokeWidth: 0
                    strokeColor: "#50000000"
                    strokeStyle: ShapePath.SolidLine
                    startX: 0
                    startY: 45
                    PathLine{
                        x: 0
                        y: 45
                    }
                    PathLine{
                        x: parent.width
                        y: 45
                    }
                }
            }

            MouseArea{

                RowLayout{
                    width: parent.width
                    height: parent.height
                    spacing: 15 //下面元素的间隔
                    x: 5

                    Text{
                        text: index+1+pageSize*pageCurrent
                        horizontalAlignment: Qt.AlignHCenter
                        Layout.preferredWidth: parent.width*0.05
                        font.family: window.mFONT_FAMILY
                        font.pointSize: 13
                        color: "black"
                        elide: Qt.ElideRight
                    }

                    Text{
                        text: name
                        Layout.preferredWidth: parent.width*0.4
                        font.family: window.mFONT_FAMILY
                        font.pointSize: 13
                        color: "black"
                        elide: Qt.ElideRight
                    }

                    Text{
                        text: artist
                        horizontalAlignment: Qt.AlignHCenter
                        Layout.preferredWidth: parent.width*0.15
                        font.family: window.mFONT_FAMILY
                        font.pointSize: 13
                        color: "black"
                        elide: Qt.ElideRight
                    }

                    Text{
                        text: album
                        horizontalAlignment: Qt.AlignHCenter
                        Layout.preferredWidth: parent.width*0.15
                        font.family: window.mFONT_FAMILY
                        font.pointSize: 13
                        color: "black"
                        elide: Qt.ElideMiddle
                    }

                    Item{
                        Layout.preferredWidth: parent.width*0.15
                        RowLayout{
                            anchors.centerIn: parent
                            MusicIconButton{
                                iconSource: "qrc:/images/pause"
                                iconHeight: 16
                                iconWidth: 16
                                toolTip: "播放"
                                onClicked: {
                                    //播放
                                }

                            }

                            MusicIconButton{
                                iconSource: "qrc:/images/favorite"
                                iconHeight: 16
                                iconWidth: 16
                                toolTip: "喜欢"
                                onClicked: {
                                    //喜欢
                                }

                            }

                            MusicIconButton{
                                iconSource: "qrc:/images/clear"
                                iconHeight: 16
                                iconWidth: 16
                                toolTip: "删除"
                                onClicked: {
                                    //删除
                                }

                            }
                        }
                    }

                }

                anchors.fill: parent
                hoverEnabled: true
                cursorShape: Qt.PointingHandCursor
                onEntered: {
                    color = "#20f0f0f0"
                }
                onExited: {
                    color = "#00000000"
                }
                onClicked: {
                    listViewDelegateItem.ListView.view.currentIndex = index
                }
            }
        }

    }

    //表格的表头
    Component{
        id: listViewHeader
        Rectangle{
            color: "#00aaaa"
            height: 45
            width: listView.width
            RowLayout{
                width: parent.width
                height: parent.height
                spacing: 15 //下面元素的间隔
                x: 5

                Text{
                    text: "序号"
                    horizontalAlignment: Qt.AlignHCenter
                    Layout.preferredWidth: parent.width*0.05
                    font.family: window.mFONT_FAMILY
                    font.pointSize: 13
                    color: "white"
                    elide: Qt.ElideRight
                }

                Text{
                    text: "歌名"
                    Layout.preferredWidth: parent.width*0.4
                    font.family: window.mFONT_FAMILY
                    font.pointSize: 13
                    color: "white"
                    elide: Qt.ElideRight
                }

                Text{
                    text: "歌手"
                    horizontalAlignment: Qt.AlignHCenter
                    Layout.preferredWidth: parent.width*0.15
                    font.family: window.mFONT_FAMILY
                    font.pointSize: 13
                    color: "white"
                    elide: Qt.ElideRight
                }

                Text{
                    text: "专辑"
                    horizontalAlignment: Qt.AlignHCenter
                    Layout.preferredWidth: parent.width*0.15
                    font.family: window.mFONT_FAMILY
                    font.pointSize: 13
                    color: "white"
                    elide: Qt.ElideMiddle
                }

                Text{
                    text: "操作"
                    horizontalAlignment: Qt.AlignHCenter
                    Layout.preferredWidth: parent.width*0.15
                    font.family: window.mFONT_FAMILY
                    font.pointSize: 13
                    color: "white"
                    elide: Qt.ElideRight
                }
            }
        }

    }

    //底部分页
    Item{
        id: pageButton
        visible: musicList.length!==0 && all!==0 //是0的话就不显示
        width: parent.width
        height: 40
        anchors.top: listView.bottom
        anchors.topMargin: 20
        ButtonGroup{
            buttons: buttons.children
        }

        RowLayout{ //一排按钮
            id: buttons
            anchors.centerIn: parent
            Repeater{
                id: repeater
                model: all/pageSize>9?9:all/pageSize
                Button{
                    Text{
                        anchors.centerIn: parent
                        text: modelData+1 //因为给model传的是数字
                        font.family: window.mFONT_FAMILY
                        font.pointSize: 11
                        color: checked?"#497563":"black"
                    }
                    background: Rectangle{
                        implicitHeight: 30
                        implicitWidth: 30
                        color: checked?"#e2f0f8":"#20e9f4ff"
                        radius: 3
                    }
                    checkable: true
                    checked: modelData===pageCurrent
                    onClicked: {
                        if(pageCurrent===index)
                            return
                        //加载数据
                        pageCurrent = index
                        loadMore(pageCurrent*pageSize, index) //1.a发送信号
                    }
                }
            }
        }
    }









}




