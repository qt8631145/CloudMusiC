import QtQuick 2.15
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQml 2.15

RowLayout{
    spacing: 0 //左边边距

    property int defaultIndex: 0
    property var qmlList: [
        {icon: "recommend-white", value: "推荐内容", qml: "DetailRecommendPageView", menu:true},
        {icon: "cloud-white", value: "搜索音乐", qml: "DetailSearchPageView", menu:true},
        {icon: "local-white", value: "本地音乐", qml: "DetailLocalPageView", menu:true},
        {icon: "history-white", value: "播放历史", qml: "DetailHistoryPageView", menu:true},
        {icon: "favorite-big-white", value: "我喜欢的", qml: "DetailFavoritePageView", menu:true},
        {icon: "", value: "", qml: "DetailPlayListPageView", menu:false} //不在菜单显示
    ]


    //左边工具栏
    Pane {

        Layout.preferredWidth: 200
        Layout.fillHeight: true
        background: Rectangle{
            color: "#aa00AAAA"
        }
        padding: 0

        ColumnLayout{
            anchors.fill: parent

            Item{
                Layout.fillWidth: true
                Layout.preferredHeight: 150

                MusicRoundImage{
                    anchors.centerIn: parent
                    height: 100
                    width: 100
                    borderRadius: 100
                }
            }

            //菜单
            ListView{
                id: menuView
                height: parent.height
                Layout.fillHeight: true
                Layout.fillWidth: true

                model: ListModel{
                    id: menuViewModel //会渲染delegate传来的窗口
                }
                delegate: menuViewDelegate
                //高亮同步
                highlight: Rectangle{
                    color: "#aa73a7ab"
                }
                //不要高亮块滑动动画效果
                highlightMoveDuration: 0
                highlightResizeDuration: 0
            }

        }


        Component{
            id: menuViewDelegate

            Rectangle{
                id: menuViewDelegateItem
                height: 50
                width: 200
                color: "#aa00AAAA"
                RowLayout{
                    anchors.fill: parent
                    anchors.centerIn: parent
                    spacing: 15
                    Item{
                        width: 30
                    }

                    Image{
                        source: "qrc:/images/"+icon //对应qmlList中的icon
                        Layout.preferredHeight: 20
                        Layout.preferredWidth: 20
                    }

                    Text{
                        text: value //对应qmlList的value
                        Layout.fillWidth: true
                        height: 50
                        font.family: window.mFONT_FAMILY
                        font.pointSize: 12
                        color: "#ffffff"
                    }
                }


                //鼠标悬停事件：改变颜色
                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: {
                        color="#aa73a7ab"
                    }
                    onExited: {
                        color="#aa00AAAA"
                    }
                    onClicked: {
                        repeater.itemAt(menuViewDelegateItem.ListView.view.currentIndex).visible = false //把之前的内容隐藏
                        //index在menuViewDelegate默认有,当前index就是qmlList的索引，currentIndex是ListView当前的索引
                        menuViewDelegateItem.ListView.view.currentIndex = index
                        //把对应的qml加载进来显示
                        var loader = repeater.itemAt(index)
                        loader.visible = true
                        loader.source = qmlList[index].qml+".qml"
                    }

                }
            }
        }

        //组件加载完成调用
        Component.onCompleted: {
            //把qmlList加载到menuViewModel
            menuViewModel.append(qmlList.filter(item=>item.menu)) //只取menu为true的
            //把对应的qml加载进来显示
            var loader = repeater.itemAt(defaultIndex)
            loader.visible = true
            loader.source = qmlList[defaultIndex].qml+".qml"

            menuView.currentIndex = defaultIndex
        }
    }

    //菜单栏
    Repeater{ //重复构造
        id: repeater
        model: qmlList.length

        Loader{
            visible: false
            Layout.fillWidth: true
            Layout.fillHeight: true
        }
    }


    //展示专辑歌单页面
    function showPlayList(targetId="", targetType="10"){
        repeater.itemAt(menuView.currentIndex).visible = false //把之前的内容隐藏
        var loader = repeater.itemAt(5)
        loader.visible = true
        loader.source = qmlList[5].qml+".qml"
        loader.item.targetId = targetId
        loader.item.targetType = targetType
    }

    function hidePlayList(){
        repeater.itemAt(menuView.currentIndex).visible = true
        var loader = repeater.itemAt(5)
        loader.visible = false
    }






}
