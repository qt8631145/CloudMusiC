import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQml 2.3

//轮播图组件
//Frame默认带一个边框
Frame{

    property int current: 0
    property alias bannerList: bannerView.model

    background: Rectangle{
        color: "#00000000"
    }

    //轮播图
    PathView{
        id: bannerView
        width: parent.width
        height: parent.height

        clip: true //超过屏幕的区域裁剪

        //鼠标悬停不播放轮播图
        MouseArea{
            anchors.fill: parent
            hoverEnabled: true
            cursorShape: Qt.PointingHandCursor
            onEntered: {
                bannerTimer.stop()
            }
            onExited: {
                bannerTimer.start()
            }
        }

        delegate: Item{
            id: delegateItem
            width: bannerView.width*0.7
            height: bannerView.height
            z: PathView.z?PathView.z:0
            scale: PathView.scale?PathView.scale:0

            MusicRoundImage{
                id: image
                imgSrc: modelData.imageUrl //数据从json传过来
                width: delegateItem.width
                height: delegateItem.height
            }

            //点击切换轮播图
            MouseArea{
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                onClicked: {
                    if(bannerView.currentIndex === index){
                        var item = bannerView.model[index]

                        //进入歌单详情页
                        var targetId = item.targetId+""
                        var targetType = item.targetType+"" //1:单曲,10:专辑,1000:歌单
                        console.log(targetId,targetType)
                        switch(targetType)
                        {
                        case "1":
                            //播放单曲

                            break
                        case "10":
                            //打开专辑
                        case "1000":
                            //打开播放列表
                            pageHomeView.showPlayList(targetId, targetType)
                            break
                        }

                    }else{
                        bannerView.currentIndex = index
                    }
                }
            }

        }

        pathItemCount: 3
        path: bannerPath
        //中间图片居中
        preferredHighlightBegin: 0.5
        preferredHighlightEnd: 0.5
    }

    Path{
        id: bannerPath
        startX: 0
        startY: bannerView.height/2-10

        PathAttribute{name:"z"; value:0}
        PathAttribute{name:"scale"; value:0.6}

        PathLine{
            x: bannerView.width/2
            y: bannerView.height/2-10
        }

        PathAttribute{name:"z"; value:2}
        PathAttribute{name:"scale"; value:0.85}

        PathLine{
            x: bannerView.width
            y: bannerView.height/2-10
        }

        PathAttribute{name:"z"; value:0}
        PathAttribute{name:"scale"; value:0.6}

    }

    //PageIndicator切换轮播图
    PageIndicator{
        id: indicator
        anchors{
            top: bannerView.bottom
            horizontalCenter: parent.horizontalCenter
            topMargin: -10 //往上10
        }

        count: bannerView.count
        currentIndex: bannerView.currentIndex
        spacing: 10
        delegate: Rectangle{
            width: 20
            height: 5
            radius: 5
            color: index===bannerView.currentIndex?"black":"grey"
            Behavior on color{

                ColorAnimation {
                    duration: 200
                }
            }

            MouseArea{
                anchors.fill: parent
                hoverEnabled: true
                cursorShape: Qt.PointingHandCursor
                onEntered: {
                    bannerTimer.stop()
                    bannerView.currentIndex = index
                }
                onExited: {
                    bannerTimer.start()
                }
            }
        }
    }


    Timer{
        id: bannerTimer
        running: true
        repeat: true
        interval: 6000 //6s
        onTriggered: {
            if(bannerView.count>0)
                bannerView.currentIndex = (bannerView.currentIndex+1)%bannerView.count
        }
    }

}
