import QtQuick 2.15
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.15

Rectangle{
    Layout.fillWidth: true
    width: parent.width
    height: 40
    color: "#00000000"

    Text{
        x: 10
        verticalAlignment: Text.AlignBottom
        text: qsTr("播放历史")
        font.family: window.mFONT_FAMILY
        font.pointSize: 25
    }
}
