import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQml 2.15


//推荐首页下的新歌推荐
Item{

    property alias lists: gridRepeater.model

    Grid{
        id: gridLayout
        anchors.fill: parent
        columns: 3

        Repeater{
            id: gridRepeater
            Frame{
                padding: 5
                width: parent.width*0.33
                height: parent.width*0.1
                background: Rectangle{
                    id: backgroud
                    color: "#00000000"
                }
                clip: true

                MusicRoundImage{
                    id: img
                    width: parent.width*0.25
                    height: parent.width*0.25
                    imgSrc: modelData.album.picUrl //Repeater里的modelData
                }

                Text{ //专辑名
                    id: name
                    anchors{
                        left: img.right
                        verticalCenter: parent.verticalCenter
                        bottomMargin: 10
                        leftMargin: 5
                    }
                    text: modelData.album.name
                    font.family: window.mFONT_FAMILY
                    font.pointSize: 11
                    height: 30
                    width: parent.width*0.72
                    elide: Qt.ElideRight //超出部分，自动省略
                }

                Text{ //歌手名
                    anchors{
                        left: img.right
                        top: name.bottom
                        leftMargin: 5
                    }
                    text: modelData.artists[0].name
                    font.family: window.mFONT_FAMILY
                    height: 30
                    width: parent.width*0.72
                    elide: Qt.ElideRight //超出部分，自动省略
                }

                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true
                    cursorShape: Qt.PointingHandCursor
                    onEntered: {
                        backgroud.color = "#50000000"
                    }
                    onExited: {
                        backgroud.color = "#00000000"
                    }
                }
            }
        }
    }
}
