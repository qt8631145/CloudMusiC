#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QIcon>
#include "httputils.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);


    //注册
    qmlRegisterType<HttpUtils>("MyUtils", 1, 0, "HttpUtils"); //主版本1，复版本0

    app.setWindowIcon(QIcon(":/images/music.png"));

    QQmlApplicationEngine engine;
    const QUrl url(u"qrc:/app.qml"_qs);



    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
        &app, [url](QObject *obj, const QUrl &objUrl) {
            if (!obj && url == objUrl)
                QCoreApplication::exit(-1);
        }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
