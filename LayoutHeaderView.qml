import QtQuick 2.15
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Window 2.15

//顶部工具栏
ToolBar{
    background: Rectangle{
        color: "#00AAAA"
    }

    width: parent.width
    Layout.fillWidth: true
    RowLayout{
        anchors.fill: parent
        //弹窗
        MusicToolButton{
            iconSource: "qrc:/images/music"
            toolTip: "关于"
            onClicked: {
                aboutPop.open()
            }
        }
        //跳转浏览器
        MusicToolButton{
            iconSource: "qrc:/images/about"
            toolTip: "nothing"
            onClicked: {
                Qt.openUrlExternally("https://www.baidu.com")
            }
        }

        //“小窗播放”按钮点击后切换“退出小窗播放”按钮
        MusicToolButton{
            id: smallWindow
            iconSource: "qrc:/images/small-window"
            toolTip: "小窗播放"
            //点击事件（js）
            onClicked: {
                setWindowSize(330, 250) //(宽，高)

                normalWindow.visible = true
                smallWindow.visible = false
            }
        }
        MusicToolButton{
            id: normalWindow
            iconSource: "qrc:/images/exit-small-window"
            toolTip: "退出小窗播放"
            visible: false
            //点击事件（js）
            onClicked: {
                setWindowSize()

                normalWindow.visible = false
                smallWindow.visible = true
            }
        }

        //顶部栏中间占位
        Item{
            Layout.fillWidth: true
            height: 32
            Text{
                anchors.centerIn: parent
                text: qsTr("")
                font.family: window.mFONT_FAMILY
                font.pointSize: 15
                color: "#ffffff"
            }
        }
        //最小化
        MusicToolButton{
            iconSource: "qrc:/images/minimize-screen"
            toolTip: "最小化"
            onClicked: {
                window.hide()
            }
        }
        //全屏
        MusicToolButton{
            id: maxWindow
            iconSource: "qrc:/images/full-screen"
            toolTip: "全屏"
            onClicked: {
                window.visibility = Window.Maximized
                maxWindow.visible = false
                resizeWindow.visible = true
            }
        }
        //退出全屏
        MusicToolButton{
            id: resizeWindow
            iconSource: "qrc:/images/small-screen"
            toolTip: "退出全屏"
            visible: false
            onClicked: {
                setWindowSize()
                window.visibility = Window.AutomaticVisibility
                maxWindow.visible = true
                resizeWindow.visible = false
            }
        }
        //退出应用
        MusicToolButton{
            iconSource: "qrc:/images/power"
            toolTip: "退出"
            onClicked: {
                Qt.quit()
            }
        }
    }

    //弹窗实现
    Popup{
        id: aboutPop
        //可以用来微调文字们居中时
        topInset: 0
        leftInset: 0
        rightInset: 0
        bottomInset: 0

        parent: Overlay.overlay
        x:(parent.width-width)/2
        y:(parent.height-height)/2

        width: 250
        height: 230

        background: Rectangle{
            color: "#e9f4ff"
            radius: 5
            border.color: "#2273a7ab"
        }

        contentItem: ColumnLayout{
            width: parent.width
            height: parent.height
            Layout.alignment: Qt.AlignHCenter

            Image {
                Layout.preferredHeight: 60
                source: "qrc:/images/music"
                Layout.fillWidth: true
                fillMode: Image.PreserveAspectFit
            }

            Text{
                text: qsTr("....")
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 18
                color: "#8573a7ab"
                font.family: window.mFONT_FAMILY
                font.bold: true
            }
            Text{
                text: qsTr("这是Cloud MusiC Player")
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 18
                color: "#8573a7ab"
                font.family: window.mFONT_FAMILY
                font.bold: true
            }
            Text{
                text: qsTr("www.baidu.com")
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 18
                color: "#8573a7ab"
                font.family: window.mFONT_FAMILY
                font.bold: true
            }
        }
    }

    function setWindowSize(width=window.mWINDOW_WIDTH, height=window.mWINDOW_HEIGHT)
    {
        window.height = height
        window.width = width
        //变化后要居中
        window.x = (Screen.desktopAvailableWidth-window.width)/2
        window.y = (Screen.desktopAvailableHeight-window.height)/2

    }














}
