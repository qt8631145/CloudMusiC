import QtQuick 2.15
import QtQuick.Controls 2.5
import Qt5Compat.GraphicalEffects

//圆角图标
Item {

    property string imgSrc: "qrc:/images/player"
    property int borderRadius: 5

    Image{
        id: image
        anchors.centerIn: parent
        source: imgSrc
        smooth: true
        visible: false
        width: parent.width
        height: parent.height
        fillMode: Image.PreserveAspectCrop //保持长宽比裁剪
        antialiasing: true //抗锯齿

    }

    Rectangle{
        id: mask
        color: "black"
        anchors.fill: parent
        radius: borderRadius
        visible: false
        smooth: true
        antialiasing: true
    }

    OpacityMask{
        anchors.fill: image
        source: image
        maskSource: mask
        visible: true
        antialiasing: true
    }
}
