import QtQuick 2.15
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.15
import QtQml 2.15


ScrollView{

    ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
    ScrollBar.vertical.policy: ScrollBar.AlwaysOn

    //ColumnLayout超出屏幕尺寸无法滚动，放在ScrollView会自动裁剪
    clip: true
    ColumnLayout {

        Rectangle{
            Layout.fillWidth: true
            width: parent.width
            height: 40
            color: "#00000000"

            Text{
                x: 10
                verticalAlignment: Text.AlignBottom
                text: qsTr("推荐内容")
                font.family: window.mFONT_FAMILY
                font.pointSize: 25
            }
        }

        MusicBannerView{
            id: bannerView
            //指定宽高
            Layout.preferredWidth: window.width-200
            Layout.preferredHeight: (window.width-200)*0.3
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        //热门歌单
        Rectangle{
            Layout.fillWidth: true
            width: parent.width
            height: 40
            color: "#00000000"

            Text{
                x: 10
                verticalAlignment: Text.AlignBottom
                text: qsTr("热门歌单")
                font.family: window.mFONT_FAMILY
                font.pointSize: 25
            }
        }

        MusicGridHotView{
            id: hotView
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.preferredHeight: (window.width-200-5*10)*0.2*4+30*4+20 //4行
            Layout.bottomMargin: 20
        }

        //新歌推荐
        Rectangle{
            Layout.fillWidth: true
            width: parent.width
            height: 40
            color: "#00000000"

            Text{
                x: 10
                verticalAlignment: Text.AlignBottom
                text: qsTr("新歌推荐")
                font.family: window.mFONT_FAMILY
                font.pointSize: 25
            }
        }

        MusicGridLatestView{
            id: latestView
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.preferredHeight: (window.width-230)*0.1*10+20 //10行
            Layout.bottomMargin: 20

        }
    }

    //网络请求banner
    Component.onCompleted: {
        getBannerList()
        //因为请求是异步的，可能只能响应一次onReply
//        getHotList()
    }

    function getBannerList(){

        function onReply(reply){ //执行getBannerList时onReply不会调用，要收到信号，才会调用
            http.onReplySignal.disconnect(onReply) //断开
            var banners = JSON.parse(reply).banners //string转成json数据
            bannerView.bannerList = banners //bannerView里的
            getHotList() //因为请求是异步的，要保证上一个请求响应后，再执行下一个请求
        }

        //on+函数名：自动绑定HttpUtils::ReplySignal
        //相当于就是用onReply准备接收信号
        http.onReplySignal.connect(onReply)
        http.connet("banner") //拿到轮播图
    }


    function getHotList(){

        function onReply(reply){ //执行getHotList时onReply不会调用，要收到信号，才会调用

            http.onReplySignal.disconnect(onReply) //断开

            var playlists = JSON.parse(reply).playlists //string转成json数据
            hotView.lists = playlists

            getLatestList()
        }

        http.onReplySignal.connect(onReply)
        http.connet("top/playlist/highquality?limit=20") //拿到精品歌单
    }


    function getLatestList(){

        function onReply(reply){ //执行getHotList时onReply不会调用，要收到信号，才会调用

            http.onReplySignal.disconnect(onReply) //断开

            var latestList = JSON.parse(reply).data //string转成json数据
            latestView.lists = latestList.slice(0,30) //取前x0项

        }

        http.onReplySignal.connect(onReply)
        http.connet("top/song") //拿到新歌速递
    }

}














