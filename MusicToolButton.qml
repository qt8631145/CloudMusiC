import QtQuick 2.15
import QtQuick.Controls 2.15

ToolButton{
    property string iconSource: ""

    property bool isCheckable: false
    property bool isChecked: false

    property string toolTip: ""

    id: self

    icon.source: iconSource

    ToolTip.visible: hovered
    ToolTip.text: toolTip

    background: Rectangle{
        color: self.down||(isCheckable&&self.checked)?"#eeeeee":"#00000000" //self.down按钮有没有被点击
    }

    icon.color: self.down||(isCheckable&&self.checked)?"#00000000":"#eeeeee"

    checkable: isCheckable
    checked: isChecked
}
